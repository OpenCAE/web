---
title: オープンCAE学会特設ページ
description:
---
## 今後のイベント

* [オープンCAE春期講習会2024]({{%relref "page/training/opencae_spring_2024_training/index.md" %}})
* [OpenFOAM GPUワークショップ2024]({{%relref "page/symposium/opencae_openfoam_gpu_workshop_2024/index.md" %}})

## 過去のイベント

* [OpenFOAM GPUトレーニング2023]({{%relref "page/training/opencae_hpc_training_gpu_2023/index.md" %}})
* [オープンCAEシンポジウム2021]({{%relref "page/symposium/opencae_symposium2021/index.md" %}})
* [オープンCAEコンテスト2021]({{%relref "page/symposium/opencae_cae_contest2021/index.md" %}})
* [オープンCAEシンポジウム2021トレーニング]({{%relref "page/training/opencae_symposium2021_training/index.md" %}})
