---
title: オープンCAEコンテスト2021
author: imano
type: page
---
* 解析対象 : ミルククラウン
* 優勝賞品 : Amazonギフト券5万円分
* 優勝発表 : 柳 光洋 ( リーディング・エッジ社 ), Smoothed CSFモデルを使用したミルククラウン解析, [発表資料](https://drive.google.com/file/d/1FkBeWvhSnrBtsb701-_Y-GWowKaMXvH9/view?usp=sharing)
* 参加資格 : オープンCAE学会の正会員および学生会員
* 参加申込 : [参加申込フォーム][1]
* 申込締切 : 2021年11月23日 ( 火 ) 17:00 ( JST ) ( 締め切りました． )
* 定員 : 10名(先着順)．定員数を超えた参加申し込みは補欠とさせて頂きます．
* 発表方法 : 発表時間4分間(質疑無し)のライトニング・トーク形式．発表時間30秒超過で発表強制終了．
* 評価方法 : 以下の6項目の合計点で評価致します．なお，各項目は5点満点です． 
	* 解析結果の妥当性検証の程度 ( 聴講者投票 )
	* 発表のわかりやすさ ( 聴講者投票 )
	* 動画・アニメーションの美しさ ( 聴講者投票 )
	* オープンソース性 ( 聴講者投票 )
	* 発表時間の誤差 ( 運営側採点．-10s以上10s以下=5点，-20s以上-10s未満=4点，-20s未満=3点，10sを越えて20s以下=2点，20sを越える=1点 )
	* 発表者種別 ( 運営側採点．学生会員=5点，正会員=4点，オープンCAEコンテスト2021講習会講師=3点，シンポジウム実行委員=2点，シンポジウム実行委員長=1点 )
* 備考 
	* 発表時に聴講者がオープンソース性を判断できるように，発表資料に使用したソフトウェアを明記ください．また，事前に発表資料をSlackに投稿ください．
	* 聴講者はセッション終了時に評価アンケートにご協力ください．
	* 2021年9月11日 ( 土 ) に開催されるオープンCAEコンテスト2021講習会において，オープンソースソフトウェアを用いた気液2相流の解析・可視化手法を説明いたしました．詳細は [オープンCAEコンテスト2021講習会のWikiページ][2] を参照ください．

 [1]: https://docs.google.com/forms/d/e/1FAIpQLSfmEGkTu_fK0E3kXS9sjoezbTAZoHc1cKmE1ESk0scf_olH8g/viewform
 [2]: https://gitlab.com/OpenCAE/OpenCAEContest2021Public/-/wikis/
