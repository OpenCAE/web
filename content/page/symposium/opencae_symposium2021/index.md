---
title: オープンCAEシンポジウム2021
author: imano
type: page
---

<div id="toc_container" class="no_bullets">
<p class="toc_title">
目次
</p>

<ol class="toc_list">
	<li>
		<a href="#general">概要</a>
	</li>
	<li>
		<a href="#importantDates">重要な期日</a>
	</li>
	<li>
		<a href="#program">講演会プログラム・概要集</a>
	</li>
	<li>
		<a href="#slides">一般講演の発表資料</a>
	</li>
	<li>
		<a href="#registration">参加申込・参加費</a>
	</li>
	<li>
		<a href="#keynote">基調講演</a>
	</li>
	<li>
		<a href="#lt">LT・拡大質疑</a>
	<li>
		<a href="#contest">オープンCAEコンテスト2021</a>
	</li>
	<li>
		<a href="#generalSession">一般講演</a>
	</li>
	<li>
		<a href="#training">トレーニング</a>
	</li>
	<li>
		<a href="#socializing">懇親会</a>
	</li>
	<li>
		<a href="#committee">実行委員会</a>
	</li>
	<li>
		<a href="#disclaimer">免責事項</a>
	</li>
	<li>
		<a href="#inquiry">お問い合わせ</a>
	</li>
</ol>
</div>

## <span id="general">概要</span>

* 開催方式 : オンライン
* 開催期日 
	* 2021年12月2日 ( 木 ) : [トレーニング][1]
	* 2021年12月3日 ( 金 ) &#8211; 4日 ( 土 ) : 講演会，[コンテスト][15]
* 協賛 : 一般社団法人 FrontISTR Commons， 一般社団法人 日本機械学会， 一般社団法人 日本計算工学会， 一般社団法人 日本流体力学会， 特定非営利活動法人 CAE懇話会 ( 五十音順 )

## <span id="importantDates">重要な期日</span>

* 講演申込開始　　 : 2021年8月7日 ( 土 )
* 講演申込締切　　 : <del datetime="2021-09-17T06:31:58+00:00">2021年9月17日 ( 金 )</del> 2021年10月1日 ( 金 ) 17:00 ( JST ) ( 延長しました．再延長はありません． )
* 講演採択通知　　 : 2021年10月4日 ( 月 )
* 早期参加申込開始 : 2021年10月4日 ( 月 )
* コンテスト参加申込開始 : 2021年10月8日 ( 金 )
* 早期参加申込締切 : 2021年11月12日 ( 金 )
* 講演梗概提出締切 : <del datetime="2021-11-05T12:50:00+09:00">2021年11月12日 ( 金 )</del><del datetime="2021-11-19T14:30:00+09:00"> 2021年11月19日 ( 金 ) 12:00 ( JST ) ( 延長しました．再延長はありません． )</del>2021年11月22日 ( 月 ) 17:00 ( JST ) ( **お詫び : 再延長しました．最終締切です．** )
* コンテスト参加申込締切 : 2021年11月23日 ( 火 ) 17:00 ( JST )
* トレーニング・講演会・懇親会参加申込締切 : 2021年11月30日 ( 火 )

## <span id="program">講演会プログラム・概要集</span>

* HTML版 : [プログラム・概要集][2]
* PDF版 : [プログラム1日目][13]，[プログラム2日目][14]，[概要集][3]

## <span id="slides">一般講演の発表資料</span>

一般講演の発表者の方から一般に公開可能なものとして提供頂いた発表資料を，順次以下に追加します．
なお，講演会プログラムにおいても，題目にリンクが貼られています．

* [一般講演の発表資料][17] 

## <span id="registration">参加申込・参加費</span>

* **[参加申込][4]** ( ユーザ登録されてない方は，「新規ユーザ登録」をお願い致します． ) **締め切りました．多数の参加申込を頂き，誠にありがとうございました．**

<table class="table table-condensed table-bordered table-striped">
<tr>
	<th style="text-align: center">
		種別
	</th>
	<th style="text-align: center;width: 15%;">
		正・賛助・公益会員
	</th>
	<th style="text-align: center;width: 15%;">
		学生会員
	</th>
	<th style="text-align: center;width: 15%;">
		一般非会員
	</th>
	<th style="text-align: center;width: 15%;">
		学生非会員
	</th>
</tr>
<tr>
	<th>
		トレーニング1，2コマ早期登録
	</th>
	<td style="text-align: right;">
		6,000円
	</td>
	<td style="text-align: right;">
		3,000円
	</td>
	<td style="text-align: right;">
		12,000円
	</td>
	<td style="text-align: right;">
		6,000円
	</td>
</tr>
<tr>
	<th>
		トレーニング1，2コマ通常登録
	</th>
	<td style="text-align: right;">
		7,000円
	</td>
	<td style="text-align: right;">
		4,000円
	</td>
	<td style="text-align: right;">
		13,000円
	</td>
	<td style="text-align: right;">
		7,000円
	</td>
</tr>
<tr>
	<th>
		トレーニング3コマ早期登録
	</th>
	<td style="text-align: right;">
		12,000円
	</td>
	<td style="text-align: right;">
		6,000円
	</td>
	<td style="text-align: right;">
		18,000円
	</td>
	<td style="text-align: right;">
		9,000円
	</td>
</tr>
<tr>
	<th>
		トレーニング3コマ通常登録
	</th>
	<td style="text-align: right;">
		13,000円
	</td>
	<td style="text-align: right;">
		7,000円
	</td>
	<td style="text-align: right;">
		19,000円
	</td>
	<td style="text-align: right;">
		10,000円
	</td>
</tr>
<tr>
	<th>
		講演会
	</th>
	<td style="text-align: right;">
		4,000円
	</td>
	<td style="text-align: right;">
		2,000円
	</td>
	<td style="text-align: right;">
		10,000円
	</td>
	<td style="text-align: right;">
		5,000円
	</td>
</tr>
<tr>
	<th>
		懇親会
	</th>
	<td style="text-align: right;">
		0円
	</td>
	<td style="text-align: right;">
		0円
	</td>
	<td style="text-align: right;">
		0円
	</td>
	<td style="text-align: right;">
		0円
	</td>
</tr>
</table>

* 協賛団体の会員の場合，講演会の参加費はオープンCAE学会の正会員と同等になりますが，トレーニングの参加費は一般非会員，または，学生非会員の扱いとなります．
* 参加費は全て非課税です．

## <span id="keynote">基調講演</span>

* 基調講演1
	* 日時 : 講演会2日目 2021年12月4日 ( 土 ) 10:05-11:20
	* 講演者名 : 青木 尊之 ( 東京工業大学 学術国際情報センター )
	* 講演題目 : 弱圧縮性計算手法による実問題の流体シミュレーション
	* 講演概要 : 計算機の高性能化に伴い、CAEで対象とする問題も複雑化・大規模化している。非圧縮性の条件を満足するためのポアソン方程式の計算は大規模問題、混相流、複雑流路で収束性が低下するため、完全陽解法で解ける弱圧縮性流体計算手法とAMR法を導入することで実問題への適用性を大きく向上させることができる。講演では、2021年3月にプレスリリースしたフォークボールの格子ボルツマン法による流体解析を始めとし、さまざまな移動境界問題、流体構造連成問題の例を示し、弱圧縮性流体解法の有用性・有効性を示す。
	* 講演資料 : [発表資料](https://opencae.gitlab.io/web/page/symposium/opencae_symposium2021/slide/K-01-slide.pdf)
* 基調講演2
	* 日時 : 講演会2日目 2021年12月4日 ( 土 ) 15:45-17:00
	* 講演者名 : 春日 悠 ( PENGUINITIS )
	* 講演題目 : PENGUINITISの作り方
	* 講演概要 : PENGUINITISを開設したのは、Linuxユーザーの端くれとしてオープンソース界隈に何かしらの恩返しがしたいという思いからでした。そのとき出会ったのがOpenFOAMでした。OpenFOAM情報の公開から書籍出版までを振り返って、オープンソースとCAEに関する情報共有と発信のありかたについて考えてみたいと思います。
	* 講演資料 : [発表資料](https://opencae.gitlab.io/web/page/symposium/opencae_symposium2021/slide/K-02-slide.pdf), [発表原稿](https://opencae.gitlab.io/web/page/symposium/opencae_symposium2021/slide/K-02.pdf)

## <span id="lt">LT・拡大質疑</span>

* テーマ ： コミュニティ活動におけるモチベーションについて
* LT発表者 ( 五十音順 )
	* カマキリ
		* サイト名 ： [宇宙に入ったカマキリ][10]
		* 自己紹介文 ： 毎日、物理を学び、役に立てないかなとブログをはじめました。同時に物理の学習サポートもしています！
	* しょちょー
		* サイト名 ： [AYTechLab][11]
		* 自己紹介文 ： 近年のパソコン性能向上に伴い市販ノートパソコンを用いて自宅で構造解析できる環境が整ってきました。本サイトでSalomeMecaの構造解析事例を公開しています。
	* ぱらぴ
		* サイト名 ： [流体力学で考える][12]
		* 自己紹介文 ： 趣味で流体現象、流体力学についての動画を制作しています。動画はYouTubeチャンネル「流体力学で考える Fluid Mechanics View」で視聴できます。

## <span id="contest">オープンCAEコンテスト2021</span>

詳細については，[こちら][15]を参照ください．

## <span id="generalSession">一般講演</span>

### <span id="theme">募集テーマなど</span>

* オープンCAEに関する応用事例，開発，カスタマイズ，普及活動，情報共有，教育・啓蒙活動など広く講演を募集します．
* 一般講演は発表時間15分，質疑3分，全体で18分とし，内容に応じて12月3日 ( 金 ) または4日 ( 土 ) のセッションに編成されます．
* 講演資格はオープンCAE学会の会員に限らず広く募集します．
* 学生 ( 社会人博士課程を除く ) の発表は「優秀学生講演賞」の審査対象となります．

### <span id="submission">講演申込</span>

* 締め切りました．多数の講演応募を頂き，誠にありがとうございました．
* 講演申込の締切は<del datetime="2021-09-17T06:31:58+00:00">2021年9月17日 ( 金 )</del> 2021年10月1日 ( 金 ) 17:00 ( JST ) です． ( 延長しました．再延長はありません．)
* なお，講演申込フォームでは，Googleフォームを用いて梗概の提出を行う関係で，Googleアカウントが必要となります．
* セキュリティポリシーなどの理由から，所属機関においてGoogleアカウントが使えない場合には，お手数ですが，シンポジウム事務局 ( <symposium@opencae.or.jp> ) までお問い合わせください
* [オープンCAEシンポジウム2021講演申込フォームのWEBページ][7]は，既に講演申込を頂いた方々の回答変更用に残してありますが，2021年10月1日17:00 ( JST ) 以降の新規の講演申込は無効となります．

### <span id="proceedings">梗概</span>

* 締め切りました．梗概を提出くださり，誠にありがとうございました．
* 梗概提出の締切は2021年11月22日 ( 月 ) 17:00 ( JST ) です． ( **お詫び : 再延長しました．最終締切です．** )
* 梗概はA4サイズ最小1ページ，最大10ページ，標準2〜4ページとします．様式などの詳細は [オープン CAE シンポジウム2021原稿の書き方][8] を参照ください．
* 梗概のテンプレートは [オープンCAE学会論文集・オープンCAEシンポジウムのテンプレートファイルのWEBページ][9] からダウンロードできます．
* 梗概のテンプレートのへッダにおける赤字の"X-1(記入後黒字に変更)"については，必ず黒字の"講演番号"に変更してくださるようお願い致します．なお，講演番号はプログラム ( [HTML版][2]， [PDF版][3] ) でご確認ください．
* 梗概pp.1のフッタにおけるcorresponding authorのメールアドレスを必ず変更くださるようお願い致します．
* 梗概における図・表のキャプションの位置は，上下どちらでも構いません．
* 梗概の提出は，[オープンCAEシンポジウム2021講演申込フォームのWEBページ][7]での回答時に送付されたメールにおける「回答の編集」のURLより行う事ができます．
* プログラムや概要集に掲載される著者氏名・所属，概要，キーワードなどの回答についても，梗概提出の締切日時まで上記から変更可能です．

### <span id="slide">発表資料</span>

* ご講演の発表資料を学会のWebページで一般に公開して良い場合には，発表資料をご提出くださるようお願い致します．
* なお，発表資料の公開の可否や公開時期はシンポジウム実行委員会に一任くださるようお願い致します．
* 発表資料の提出は，[オープンCAEシンポジウム2021講演申込フォームのWEBページ][7]での回答時に送付されたメールにおける「回答の編集」のURLより行う事ができます．

## <span id="training">トレーニング</span>

詳細については，[こちら][1]を参照ください．

## <span id="socializing">懇親会</span>

* 日時 : 講演会2日目 2021年12月4日 ( 土 ) 18:00-20:00
* セッション終了後，オンライン会議システム[Gather.Town][16]を用いた懇親会を開催いたします．
* オープンCAE業界の交流の場としてご活用ください．
* トレーニングや講演会のイベント登録者は無料でご参加いただけます．
* 懇親会参加申込締切 : 2021年11月30日 ( 火 )
* **[参加申込][4]** ( ユーザ登録されてない方は，「新規ユーザ登録」をお願い致します． ) **締め切りました．多数の参加申込を頂き，誠にありがとうございました．**

* 備考
	* 懇親会参加用URLは懇親会に参加申込した方のみにメールで送付致します．
	* 各自飲み物や食べ物を持参して，お気軽にご参加ください．

## <span id="committee">実行委員会</span>

* 実行委員長 ： 今野 雅 ( OCAEL )
* 実行委員 : 秋山 善克， 大島 聡史 ( 名古屋大学 )， 川畑 真一 ( オープンCAE勉強会＠関西 )， 酒井 秀久 ( オープンCAE学会 )， 高木 洋平 ( 横浜国立大学 )， 龍野 潤， 田村 守淑， 中川 慎二 ( 富山県立大学，オープンCAE学会会長 )， 西 剛伺 ( 足利大学 )， 平野 博之 ( 岡山理科大学 )， 福江 高志 ( 金沢工業大学 ) ( 五十音順 )
* シンポジウム事務局 ： デグチ企画

## <span id="disclaimer">免責事項</span>

* 最新情報につきましては，本ページにて随時更新していきます．
* 本ページの内容は公開時点でのものであり，今後一部の内容・予定を予告なく変更する場合があります．
* また，最少催行人数未達や会議システム等の制約から，行事の一部を中止または先着順で受付を締め切る場合があります．

## <span id="inquiry">お問い合わせ</span>

本行事に関するご質問は， シンポジウム事務局 ( <symposium@opencae.or.jp> ) までお送りください．

 [1]: {{%relref "page/training/opencae_symposium2021_training/index.md" %}}
 [2]: https://docs.google.com/spreadsheets/d/e/2PACX-1vR9SLzQkEWszUzQBkxOOniyKBp97L0q1W1Q-D1gGfDawcr2pW-6ZEYYjyx3DMwTG4dvh-ye6mcHG2Fo/pubhtml?gid=2136031643#
 [3]: https://docs.google.com/spreadsheets/d/e/2PACX-1vR9SLzQkEWszUzQBkxOOniyKBp97L0q1W1Q-D1gGfDawcr2pW-6ZEYYjyx3DMwTG4dvh-ye6mcHG2Fo/pub?gid=1985234918&single=true&output=pdf
 [4]: https://m7.members-support.jp/opencae/Entrys/event_detail_entry/63
 [5]: https://docs.google.com/forms/d/e/1FAIpQLSfmEGkTu_fK0E3kXS9sjoezbTAZoHc1cKmE1ESk0scf_olH8g/viewform
 [6]: http://www.opencae.or.jp/activity/training/opencae_contest2021_training/
 [7]: https://docs.google.com/forms/d/e/1FAIpQLSfuxuZRvz6W16k3cdt6bJ0HMlE5TLWWlog36_2MUYt1DoqYfg/viewform
 [8]: https://gitlab.com/OpenCAE/template_OpenCAE_journal_and_symposium/-/raw/master/symposium/TeX/template_OpenCAE_symposium.pdf
 [9]: https://gitlab.com/OpenCAE/template_OpenCAE_journal_and_symposium/-/wikis/home
 [10]: https://takun-physics.net/
 [11]: https://aytechlab.com/
 [12]: https://www.youtube.com/channel/UCzGeixkU8YhmHqnCnU2FoUQ
 [13]: https://docs.google.com/spreadsheets/d/e/2PACX-1vR9SLzQkEWszUzQBkxOOniyKBp97L0q1W1Q-D1gGfDawcr2pW-6ZEYYjyx3DMwTG4dvh-ye6mcHG2Fo/pub?gid=2136031643&single=true&output=pdf
 [14]: https://docs.google.com/spreadsheets/d/e/2PACX-1vR9SLzQkEWszUzQBkxOOniyKBp97L0q1W1Q-D1gGfDawcr2pW-6ZEYYjyx3DMwTG4dvh-ye6mcHG2Fo/pub?gid=848489750&single=true&output=pdf
 [15]: {{%relref "page/symposium/opencae_cae_contest2021/index.md" %}}
 [16]: https://www.gather.town/
 [17]: https://drive.google.com/drive/folders/1ZuOPqxNPotHLcLEyS7cAa7iAua0a6zZZ
