---
title: OpenFOAM GPUトレーニング2023
author: imano
type: page
---
<div id="toc_container" class="no_bullets">
<p class="toc_title">
目次
</p>
<ol class="toc_list">
	<li>
		<a href="#overview">概要</a>
	</li>
	<li>
		<a href="#member">実行委員</a>
	</li>
	<li>
		<a href="#program">プログラム</a>
	</li>
	<li>
		<a href="#note">備考</a>
	</li>
	<li>
		<a href="#registration">参加申込</a>
	</li>
	<li>
		<a href="#disclaimer">免責事項</a>
	</li>
	<li>
		<a href="#inquiry">お問い合わせ</a>
	</li>
</ol>
</div>

## <span id="overview">概要</span>

* 日時: 2023年9月9日(土) 10:00-17:30
* 開催形態: [Zoom][1] を用いたオンライン
* 講習題目: Google Colaboratoryを用いたOpenFOAM実行とGPU化に向けて
* 定員: 20名 (先着順．最少催行人数5名)
* 参加費: 無料
* 参加資格: オープンCAE学会の正会員または学生会員  
* ターゲット
	* [Google Colaboratory][2](以下，Colab)での[OpenFOAM][3]の実行方法に興味がある方
	* GPUを用いたOpenFOAM・[RapidCFD][4]による解析や高速化に興味がある方

## <span id="member">実行委員</span>

* 大島 聡史 (実行委員長，オープンCAE学会HPC小委員会委員長，九州大学情報基盤研究開発センター)
* 今野 雅 (幹事，オープンCAE学会HPC小委員会委員，東京大学情報基盤センター客員研究員)
* 川畑 真一 (オープンCAE学会)
* 出川 智啓 (オープンCAE学会)
* 森本 賢治 (オープンCAE学会)

敬称略，順不同

## <span id="program">プログラム</span>

* 09:30-10:00 「開場」
* 10:00-10:10 「オープニング」 大島 聡史(前出)
* 10:10-10:30 「講習会の準備」(演習) 今野 雅(前出) ([演習資料][9])
	* Colabへの接続
	* Google Driveのマウント
	* 講習ファイルのダウンロード
* 10:30-12:00 「GPUプログラミングの基礎」(講演) 大島 聡史(前出) ([講演資料][10])
* 12:00-13:00 「休憩」
* 13:00-13:30 「RapidCFDの高速化事例紹介」(講演) 山岸 孝輝(高度情報科学技術研究機構)
* 13:30-17:00 「Google ColaboratoryのCPU・GPUを用いた3次元キャビティ・ベンチマークテスト実行」(演習) 今野 雅(前出) ([演習資料][9])
	* イントロダクション
	* HPC Techinical Committeeの3次元キャビティ解析ベンチマークテスト
	* OpenFOAM-v2306を用いたベンチマークテスト
	* 休憩
	* RapidCFDを用いたベンチマークテスト
	* プロファイリング
	* カスタマイズ演習
* 17:00-17:25 「全体質疑」
* 17:25-17:30 「クロージング」 大島 聡史(前出)

敬称略
## <span id="note">備考</span>

* 演習では[Zoom][1]，および，[Google Colaboratory][2](Colab)，[Google Drive][5]を用いますので，演習当日これらに接続できる環境が必要です．
* Zoomに関しては，予め[Zoomミーティングシステムの接続テスト][6]を実施し，接続可能であることをご確認ください．
* オープンCAE学会の正会員または学生会員でない方が申し込みされる場合には，申込前に[入会・各種お手続き][7]より入会をお願い致します．

## <span id="registration">参加申込</span>

* [参加申込][8] **(定員に達したため，締め切りました．)**

## <span id="disclaimer">免責事項</span>

* 最新情報につきましては，本ページにて随時更新していきます．
* 本ページの内容は公開時点でのものであり，今後一部の内容・予定を予告なく変更する場合があります．
* また，最少催行人数未達や会議システム等の制約から，行事の一部を中止または先着順で受付を締め切る場合があります．
* 本研修の受講にはインターネット接続が必要です．接続に係る通信料は受講者各自の負担とします．
* 受講者の各自が最新のコンピュータウィルス対策等がなされている機器を使用し，Zoom 最新バージョンにて受講してください．
* 講演者，主催者は，Zoom インストールや本研修受講によるコンピュータウィルスや第三者の妨害等行為による不可抗力によって生じた損害等の一切の責任を負いませんのでご了承ください．
* Zoom の使用方法・操作方法については，講演者，主催者では個別のサポートはいたしかねますのでご了承ください．

## <span id="inquiry">お問い合わせ</span>

* 本行事に関するご質問は，オープンCAE学会事務局 ( <office@opencae.or.jp> ) までお送りください．

 [1]: https://zoom.us/
 [2]: https://colab.research.google.com/
 [3]: https://www.openfoam.com/
 [4]: https://sim-flow.com/rapid-cfd-gpu/
 [5]: https://www.google.com/intl/ja_jp/drive/
 [6]: https://zoom.us/test
 [7]: https://www.opencae.or.jp/procedure/
 [8]: https://docs.google.com/forms/d/e/1FAIpQLSeP_c4KnpriojUdQxYfLx6p0bbKx-h-Gri7UHpko6WO62S0Kw/viewform
 [9]: https://github.com/opencae/hpc-training-gpu-2023/tree/main
 [10]: https://drive.google.com/file/d/1SV19IWV_r7CEzM52QQ1p-1i13WLK5F-f/view