---
title: オープンCAE春期講習会2024
author: imano
type: page
---
<div id="toc_container" class="no_bullets">
<p class="toc_title">
目次
</p>
<ol class="toc_list">
	<li>
		<a href="#overview">概要</a>
	</li>
	<li>
		<a href="#program">プログラム</a>
	</li>
	<li>
		<a href="#note">備考</a>
	</li>
	<li>
		<a href="#registration">参加申込</a>
	</li>
	<li>
		<a href="#disclaimer">免責事項</a>
	</li>
	<li>
		<a href="#inquiry">お問い合わせ</a>
	</li>
</ol>
</div>

## <span id="overview">概要</span>

* 日時 : 2024年3月20日(水，祝) 09:30-12:00
	* 同日の午後に[OpenFOAM GPUワークショップ2024]({{%relref "page/symposium/opencae_openfoam_gpu_workshop_2024/index.md" %}})が開催されます．
* 開催形態 : [Zoom][1] を用いたオンライン
* 講師 : 田村 守淑 (オープン科学計算コンサルティング)
* 講習題目 : OpenFOAMでの剛体と流体運動の連成シミュレーション
* 定員 : 20名 (先着順．最少催行人数5名)
* 参加費 : 学生会員 無料, 非会員学生 3,000円, 社会人会員 3,000円, 社会人非会員 6,000円

## <span id="program">プログラム</span>

* 09:00-09:30 オンライン接続テスト・事前準備相談
* 09:30-10:40 メッシュモーフィング編
	* interFoamのチュートリアルfloatingObjectをベースとして，２つの剛体運動ライブラリ (sixDoFRigidBodyMotion, rigidBodyMeshMotion) の基本的な設定方法の解説と演習
	* sixDoFRigidBodyMotionとrigidBodyMeshMotionの比較を行う
	* その他の話題 (複数物体の設定など)
* 10:40-10:50 休憩
* 10:50-12:00 オーバーセットメッシュ編
	* overInterDyMFoamのチュートリアルfloatingBodyをベースとして，オーバーセットメッシュとsixDoFRigidBodyMotionの基本的な設定方法の解説と演習
	* メッシュモーフィングとの比較を行う
	* その他の話題 (並列計算のベンチマークの紹介など)

## <span id="note">備考</span>

* 演習では[Zoom][1]，および，[Google Colaboratory][2](Colab)，[Google Drive][3]を用いますので，演習当日これらに接続できる環境が必要です．
* Zoomに関しては，予め[Zoomミーティングシステムの接続テスト][4]を実施し，接続可能であることをご確認ください．

## <span id="registration">参加申込</span>

* [参加申込][5]

## <span id="disclaimer">免責事項</span>

* 最新情報につきましては，本ページにて随時更新していきます．
* 本ページの内容は公開時点でのものであり，今後一部の内容・予定を予告なく変更する場合があります．
* また，最少催行人数未達や会議システム等の制約から，行事の一部を中止または先着順で受付を締め切る場合があります．
* 本研修の受講にはインターネット接続が必要です．接続に係る通信料は受講者各自の負担とします．
* 受講者の各自が最新のコンピュータウィルス対策等がなされている機器を使用し，Zoom 最新バージョンにて受講してください．
* 講演者，主催者は，Zoom インストールや本研修受講によるコンピュータウィルスや第三者の妨害等行為による不可抗力によって生じた損害等の一切の責任を負いませんのでご了承ください．
* Zoom の使用方法・操作方法については，講演者，主催者では個別のサポートはいたしかねますのでご了承ください．

## <span id="inquiry">お問い合わせ</span>

* 本行事に関するご質問は，オープンCAE学会事務局 ( <office@opencae.or.jp> ) までお送りください．

 [1]: https://zoom.us/
 [2]: https://colab.research.google.com/
 [3]: https://www.google.com/intl/ja_jp/drive/
 [4]: https://zoom.us/test
 [5]: https://m7.members-support.jp/opencae/login/