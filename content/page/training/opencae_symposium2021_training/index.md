---
title: オープンCAEシンポジウム2021トレーニング
author: imano
type: page
---

<div id="toc_container" class="no_bullets">
<p class="toc_title">
目次
</p>

<ol class="toc_list">
	<li>
		<a href="#program">プログラム</a>
	</li>
	<li>
		<a href="#registration">参加申込・参加費</a>
	</li>
	<li>
		<a href="#disclaimer">免責事項</a>
	</li>
	<li>
		<a href="#inquiry">お問い合わせ</a>
	</li>
</ol>
</div>

## <span id="program">プログラム</span>

<table class="table table-condensed table-bordered table-striped">
<tr>
	<th style="text-align: center;">
		2021年12月2日
	</th>
	<th style="text-align: center;width: 30%;">
		会場A
	</th>
	<th style="text-align: center;width: 30%;">
		会場B
	</th>
	<th style="text-align: center;width: 30%;">
		会場C
	</th>
</tr>
<tr>
	<th>
		10:00-<br />10:30
	</th>
	<td colspan="4">
		受付
	</td>
</tr>
<tr>
	<th>
		10:30-<br />12:00
	</th>
	<td>
		A1 : 中川慎二<br />( 富山県立大学 )<br /><strong>OpenFOAMによる熱流体シミュレーション入門</strong><br /><br /><a href="../opencae_symposium2021_training_a1_a2/">詳細ページ</a><br /><strong>(定員に達したため受付終了)</strong>
	</td>
	<td>
		B1 : 坪田 遼<br />( XSim )<br /><strong>モデリングトレーニング基礎からのFreeCAD</strong><br /><br /><a href="../opencae_symposium2021_training_b1/">詳細ページ</a>
	</td>
	<td>
		C1 : 稲葉竜一<br /><strong>PythonによるOpenFOAMのハンドリング （ 題材： 形状や計算条件のベイズ最適化 ）</strong><br /><br /><a href="../opencae_symposium2021_training_c1/">詳細ページ</a><br /><strong>(定員に達したため受付終了)</strong>
	</td>
</tr>
<tr>
	<th>
		12:00-<br />13:00
	</th>
	<td colspan="3">
		休憩
	</td>
</tr>
<tr>
	<th>
		13:00-<br />14:30
	</th>
	<td>
		A2 : 中川慎二<br />( 富山県立大学 )<br /><strong>OpenFOAMによる熱流体シミュレーション初級</strong><br /><br /><a href="../opencae_symposium2021_training_a1_a2/">詳細ページ</a><br /><strong>(定員に達したため受付終了)</strong>
	</td>
	<td>
		B2 : 川畑 真一<br />( オープンCAE勉強会@関西)<br /><strong>Code-Asterユーザ定義材料の使い方</strong><br /><br /><a href="../opencae_symposium2021_training_b2/">詳細ページ</a>
	</td>
	<td>
		C2 : zeta_plusplus<br />( Modelica勉強会) <strong>OpenModelicaによる流体システムモデリング (初級者向け)</strong><br /><br /><a href="../opencae_symposium2021_training_c2/">詳細ページ</a>
	</td>
</tr>
<tr>
	<th>
		14:30-<br />15:00
	</th>
	<td colspan="3">
		休憩
	</td>
</tr>
<tr>
	<th>
		15:00-<br />16:30
	</th>
	<td>
		A3 : 野村 悦治<br />( OCSE^2 )<br /><strong>はじめてのDEXCS2021 for OpenFOAM</strong><br /><br /><a href="../opencae_symposium2021_training_a3/">詳細ページ</a>
	</td>
	<td>
		B3 : 柴田良一<br />( 岐阜高専 )<strong><br />Elmerではじめる連成解析</strong><br /><br /><a href="../opencae_symposium2021_training_b3/">詳細ページ</a>
	</td>
	<td>
		C3 : 山本 卓也<br />( 東北大学 )<br /><strong>OpenFOAMにおける相変化解析</strong><br /><br /><a href="../opencae_symposium2021_training_c3/">詳細ページ</a><br /><strong>(定員に達したため受付終了)</strong>
	</td>
</tr>
<tr>
	<th>
		16:30-<br />17:00
	</th>
	<td colspan="3">
		サポート
	</td>
</tr>
</table>

* **日本機械学会計算力学技術者認定受験資格の認定証の取得希望の方は，A1「OpenFOAMによる熱流体シミュレーション入門」と，A2「OpenFOAMによる熱流体シミュレーション初級」をどちらも受講する必要があります．**
* 時間が重複していなければ，A1, A2, C3 のようにAとCのような別ルームのコースを複数選択して申し込みすることが可能です．
* いずれのルームも各コース定員20名，最小催行人数5名です．
* また定員に達した場合はお申し込みをお断りする場合があります．
* **全コースを予定通り実施予定です(11/26追記).**

## <span id="registration">参加申込・参加費</span>

* **[参加申込][4]** ( ユーザ登録されてない方は，「新規ユーザ登録」をお願い致します． ) **締め切りました．多数の参加申込を頂き，誠にありがとうございました．**

<table class="table table-condensed table-bordered table-striped">
<tr>
	<th style="text-align: center">
		種別
	</th>
	<th style="text-align: center;width: 15%;">
		正・賛助・公益会員
	</th>
	<th style="text-align: center;width: 15%;">
		学生会員
	</th>
	<th style="text-align: center;width: 15%;">
		一般非会員
	</th>
	<th style="text-align: center;width: 15%;">
		学生非会員
	</th>
</tr>
<tr>
	<th>
		トレーニング1，2コマ早期登録
	</th>
	<td style="text-align: right;">
		6,000円
	</td>
	<td style="text-align: right;">
		3,000円
	</td>
	<td style="text-align: right;">
		12,000円
	</td>
	<td style="text-align: right;">
		6,000円
	</td>
</tr>
<tr>
	<th>
		トレーニング1，2コマ通常登録
	</th>
	<td style="text-align: right;">
		7,000円
	</td>
	<td style="text-align: right;">
		4,000円
	</td>
	<td style="text-align: right;">
		13,000円
	</td>
	<td style="text-align: right;">
		7,000円
	</td>
</tr>
<tr>
	<th>
		トレーニング3コマ早期登録
	</th>
	<td style="text-align: right;">
		12,000円
	</td>
	<td style="text-align: right;">
		6,000円
	</td>
	<td style="text-align: right;">
		18,000円
	</td>
	<td style="text-align: right;">
		9,000円
	</td>
</tr>
<tr>
	<th>
		トレーニング3コマ通常登録
	</th>
	<td style="text-align: right;">
		13,000円
	</td>
	<td style="text-align: right;">
		7,000円
	</td>
	<td style="text-align: right;">
		19,000円
	</td>
	<td style="text-align: right;">
		10,000円
	</td>
</tr>
<tr>
	<th>
		講演会
	</th>
	<td style="text-align: right;">
		4,000円
	</td>
	<td style="text-align: right;">
		2,000円
	</td>
	<td style="text-align: right;">
		10,000円
	</td>
	<td style="text-align: right;">
		5,000円
	</td>
</tr>
<tr>
	<th>
		懇親会
	</th>
	<td style="text-align: right;">
		0円
	</td>
	<td style="text-align: right;">
		0円
	</td>
	<td style="text-align: right;">
		0円
	</td>
	<td style="text-align: right;">
		0円
	</td>
</tr>
</table>

* 協賛団体の会員の場合，講演会の参加費はオープンCAE学会の正会員と同等になりますが，トレーニングの参加費は一般非会員，または，学生非会員の扱いとなります．
* 参加費は全て非課税です．

## <span id="disclaimer">免責事項</span>

* 本研修の受講にはインターネット接続が必要です．接続に係る通信料は受講者各自の負担とします．
* 受講者の各自が最新のコンピュータウィルス対策等がなされている機器を使用し，Zoom 最新バージョンにて受講してください．
* 講演者，主催者は，Zoom インストールや本研修受講によるコンピュータウィルスや第三者の妨害等行為による不可抗力によって生じた損害等の一切の責任を負いませんのでご了承ください．
* 本研修への受講申込に際しては，必ず Zoom ミーティングシステムの接続テストを実施し，視聴可能であることをご確認の上，お申し込みください． ( [Zoom 公式サイト・テストミーティング][1] )
* Zoom の使用方法・操作方法については，講演者，主催者では個別のサポートはいたしかねますのでご了承ください．
* 主催者側都合で急遽オンライン講習を中止する場合は返金処理を行いますが，認定証は発行いたしません．なお，発行できない事に対する一切の責任を追わないものとします．

## <span id="inquiry">お問い合わせ</span>

本行事に関するご質問は， シンポジウム事務局 ( <symposium@opencae.or.jp> ) までお送りください．

 [1]: https://zoom.us/test
 [4]: https://m7.members-support.jp/opencae/Entrys/event_detail_entry/63
