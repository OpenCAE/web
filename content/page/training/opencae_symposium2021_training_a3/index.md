---
title: オープンCAEシンポジウム2021トレーニングA3
author: imano
type: page
---
## A3 : はじめてのDEXCS2021 for OpenFOAM

* 講師 : 野村 悦治 ( オープンCAEコンサルタントOCSE^2 )
* 講習概要 : DEXCS for OpenFOAM はOpenFOAMを「誰にでも簡単，すぐに使える」を目的に開発されたオールフリー＆オープンのオールインワンパッケージシステムで，初心者向けの自習ツールとしての利用から始めて，これを使いこなせるようになれば実践的な解析環境としても実用できる．本講習では使いこなす為の勘所を演習解説する．
* 講習内容
	1. DEXCS for OpenFOAMの狙い
		* 市販ソフトとはGUIコンセプトが違う
		* OpenFOAMの知識、実践的活用法の理解
	1. 実践的活用法とは
		* メッシュ作成とソルバー設定は別物
	1. プリ処理の勘所
		* FreeCAD, cfMesh, Dexcsマクロ/WB
	1. ソルバー設定の勘所
		* TreeFoam, 標準チュートリアル
	1. ポスト処理の勘所
		* ParaView, Dexcsプロットツール
 * 受講にあたって必要な環境
	* DEXCS2021 for OpenFOAMを利用いたします．
	* [DEXCSダウンロードサイト][1]，または，[オープンCAEシンポジウム2021トレーニング用仮想マシンダウンロードサイト][2]から，isoイメージファイルを入手し，仮想マシンを構築してください．
	* 仮想マシン構築について以下などを参照下さい．
		* スライド [仮想マシンの構築法][3]
		* 書籍 [森北出版「OpenFOAMによる熱移動と流れの数値解析」][4](ISBN978-4-627-69102-5)の付録A
		* 書籍 [丸善出版「オープンCAEのためのDEXCS for OpenFOAM ハンドブック」][5](ISBN978-4-621-30613-0)の付録A
	* なおVirtulBoxを使用する場合に正常にログアウトできないなどの問題点があります．
	* 対応などの詳細は[仮想マシンの構築法][3]についてを参照ください．

 [1]: http://dexcs.gifu-nct.ac.jp/
 [2]: https://gitlab.com/OpenCAE/Symposium2021Training
 [3]: https://www.slideshare.net/etsujinomura/dexcs2021-of-install2-250650430
 [4]: https://www.morikita.co.jp/books/mid/069102
 [5]: https://www.maruzen-publishing.co.jp/item/?book_no=304171
