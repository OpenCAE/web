---
title: オープンCAEシンポジウム2021トレーニングB2
author: imano
type: page
---
## B2 : Code-Asterユーザ定義材料の使い方
* 講師 : 川畑 真一 （ オープンCAE勉強会@関西 ）
* 講習概要 : オープンソースの構造解析コードであるCode-Asterは商用の構造解析ソフトウェアでできる計算のほとんどが実行できます。本講習ではCode-Asterで利用できる応用的な機能紹介として、ユーザーサブルーチン機能（UMAT）について概要から計算投入方法まで説明します．計算投入はSalome-Mecaを使った基本的なインプットの準備の仕方から説明するため、初心者の方でもSalome-Mecaの使った構造解析の基本的な実行方法から学ぶことができます．
* 受講にあたって必要な環境
	* 仮想マシンovaファイルを配布します．VirtualBox6.1.26で動作を確認しています。
	* Salome-Mecaはv2019を使用します。Code‐Asterのバージョンは現行のstable版であるv14.4を使います。
	* VirtualBoxの開発元方針により，M1 Macは対象外です．

### 講習内容
本講習ではまずSalome-Mecaの使い方を紹介します。最近のSalome-Mecaでは形状作成機能がよりCADに近くなったShaperモジュールが追加されております。本講習会ではこのShaperモジュールを使った形状作成、メッシュ生成、条件設定をSalome-Mecaで実施し、Code-Asterを使ったコマンドによる計算投入を行います。

![ShaperModule](figB2-1.png)
Shaperモジュールでの形状作成．

後半ではCode-Asterの持つ応用的な機能のうち、日本語の情報が少ない材料サブルーチンUMATを使った計算方法について説明します。

![ShaperModule](figB2-2.png)

### 環境構築について

講習で使用する環境のovaファイルは次のフォルダからダウンロードしてください。（いずれかアクセスできる方からダウンロードください。）

<a href="https://drive.google.com/drive/folders/1m06CkWi41OL32RLSQBiAigpdsazvlUiY?usp=sharing">ovaファイルのダウンロード(Google Drive)</a>

<a href="https://www.dropbox.com/sh/oinon20dx8t2184/AACP-s71BGpYJVfs4X9Vu3v2a?dl=0">ovaファイルのダウンロード(Dropbox)</a>

<a href="https://gitlab.com/OpenCAE/Symposium2021TrainingB2">ovaファイルのダウンロード(GitLab)</a>

講習環境の構築はこちらの手順を参照してください。

<a href="講習会環境について.pdf">講習会環境について</a>
