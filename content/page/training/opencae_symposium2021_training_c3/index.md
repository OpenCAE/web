---
title: オープンCAEシンポジウム2021トレーニングC3
author: imano
type: page
---
### C3 : OpenFOAMにおける相変化解析
* 講師 : 山本 卓也 ( 東北大学 )
* 講習概要 : OpenFOAMのfvOptions内の固液相変化機能の利用方法と設定方法，そのモデルの背景について解説，及び演習を行います．
* 受講にあたって必要な環境
	* A3と同様のDEXCS2021 for OpenFOAM環境
	* もしくは，ESI版OpenFOAM (v1612+ ～ v2106)を実行できるPC環境を準備ください(OpenFOAM-v1812, OpenFOAM-v2012, OpenFOAM-v2106で動作検証済)．インストール方法等は[ESI版OpenFOAM][1]をご覧ください．
	* 不明点があれば事前にお知らせください．
	

 [1]: https://www.openfoam.com/current-release
